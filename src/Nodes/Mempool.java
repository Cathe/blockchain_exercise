package Nodes;

import Transactions.Transaction;
import Transactions.TransactionComparator;

import java.util.ArrayList;
import java.util.List;

public class Mempool {

    private List<Transaction> pendingTransactions;

    public Mempool(){
        pendingTransactions = new ArrayList<>();
    }

    public void addTransaction(Transaction t){

        pendingTransactions.add(t);

        pendingTransactions.sort(new TransactionComparator());
    }

    public void removeTransaction(Transaction t) {
        pendingTransactions.remove(t);
    }

    public List<Transaction> getPendingTransactions() {
        return pendingTransactions;
    }


}
