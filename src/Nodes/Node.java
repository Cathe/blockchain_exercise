package Nodes;

import Block.Block;
import Network.Network;
import Transactions.Transaction;

import java.util.ArrayList;
import java.util.List;

public class Node implements Observer {

    protected List<Block> blockchain = new ArrayList<>();
    protected Mempool mempool = new Mempool();
    protected Network network;

    public Node(Network network){
        this.network = network;

        network.register(this);
    }

    public void upload(Node node){

        blockchain = new ArrayList<>();
        mempool = new Mempool();


        blockchain.addAll(node.getBlockchain());

        for (Transaction transaction : node.getMempool().getPendingTransactions()) {
            mempool.addTransaction(transaction);
        }

    }


    @Override
    public void updateBlockchain(Block addedBlock) {

        if(blockchain.size() > 0){
            String previousHash = blockchain.get(blockchain.size()-1).getHash();

            if(!addedBlock.getPreviousBlockHash().contentEquals(previousHash))
                return;
        }



        blockchain.add(addedBlock);

        for (Transaction t : addedBlock.getData()) {
            mempool.removeTransaction(t);
        }
    }

    @Override
    public void updateMempool(Transaction addedTransaction) {

        mempool.addTransaction(addedTransaction);
    }

    public List<Block> getBlockchain() {
        return blockchain;
    }

    public Mempool getMempool() {
        return mempool;
    }

    @Override
    public String toString() {

        for (Block b : blockchain) {
            System.out.println(b);
        }

        return "";
    }
}
