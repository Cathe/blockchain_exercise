package Nodes;

import Network.Network;
import Transactions.Transaction;

public class TransactionPlatform extends Node {

    public TransactionPlatform(Network network) {
        super(network);
    }

    public void addTransaction(Transaction t){
        mempool.addTransaction(t);

        network.notifyTransactionAdded(t);
    }
}
