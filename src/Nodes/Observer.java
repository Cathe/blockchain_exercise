package Nodes;

import Block.Block;
import Transactions.Transaction;

public interface Observer {

    void updateBlockchain(Block addedBlock);
    void updateMempool(Transaction addedTransaction);
}
