package Nodes;

import Block.Block;
import Block.BlockchainSystem;
import Network.Network;
import Transactions.Transaction;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Miner extends Node {

    private String name;

    public Miner(String name, Network network) {
        super(network);

        this.name = name;
    }


    public void mine(){

        BlockchainSystem blockchainSystem = new BlockchainSystem();

        List<Transaction> transactions = getTransactionsFromMempool(blockchainSystem.getNumberOfTransactionPerBlock());


        String previousHash = "";
        if(blockchain.size() > 0)
            previousHash = blockchain.get(blockchain.size()-1).getHash();



        Block block = new Block(
                blockchainSystem.getBlocNumber(),
                previousHash,
                getMerkleRoot(transactions),
                new Date().getTime(),
                blockchainSystem.getDifficulty(),
                transactions
        );


        findHash(block);

    }

    private void findHash(Block block) {

        String header = block.getPreviousBlockHash() +
                block.getMerkleRoot() + block.getTimeStamp() + block.getDifficulty();

        int difficulty = block.getDifficulty();
        long nonce = 0;

        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            System.exit(1);
        }

        BigInteger target = BigInteger.TWO.pow(8*digest.getDigestLength()-difficulty);

        long startTime = System.nanoTime();

        byte[] hash;
        BigInteger biHash;

        do {

            digest.reset();

            ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
            buffer.putLong(nonce);

            digest.update(header.getBytes(StandardCharsets.UTF_8));
            digest.update(buffer.array());

            hash = digest.digest();

            biHash = new BigInteger(1, hash);


            nonce++;

        }while(biHash.compareTo(target) >= 0);



        StringBuilder result = new StringBuilder();
        for (int i = 0; i < hash.length; ++i) {
            result.append(Integer.toHexString((hash[i] & 0xFF) | 0x100).substring(1, 3));
        }

        block.setHash(result.toString());
        block.setNonce(nonce);

        network.notifyBlockAdded(block);

        long endTime = System.nanoTime();


        System.out.println("Hash found in " + (endTime-startTime)/1000 + " microseconds : " + result);
    }

    private String getMerkleRoot(List<Transaction> transactions) {

        List<String> tree = new ArrayList<>();

        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            System.exit(1);
        }

        for (int i = 0; i < transactions.size()-1; i += 2) {
            digest.update(transactions.get(i).getBytes(StandardCharsets.UTF_8));
            digest.update(transactions.get(i+1).getBytes(StandardCharsets.UTF_8));

            byte[] hash = digest.digest();
            tree.add(Arrays.toString(hash));
        }

        while(tree.size() > 1){

            for (int i = 0; i < tree.size()-1; i++) {
                digest.update(tree.get(i).getBytes(StandardCharsets.UTF_8));
                tree.remove(i);
                digest.update(tree.get(i).getBytes(StandardCharsets.UTF_8));
                tree.remove(i);

                byte[] hash = digest.digest();

                StringBuilder merkle = new StringBuilder();
                for (int j = 0; j < hash.length; ++j) {
                    merkle.append(Integer.toHexString((hash[j] & 0xFF) | 0x100).substring(1, 3));
                }
                tree.add(i, merkle.toString());

            }
        }

        return tree.get(0);
    }

    private List<Transaction> getTransactionsFromMempool(int numberOfTransactionPerBlock) {

        List<Transaction> transactions = new ArrayList<>();
        int rewardFee = 0;
        double reward = new BlockchainSystem().getMinningReward();

        if(mempool.getPendingTransactions().size() < numberOfTransactionPerBlock)
            numberOfTransactionPerBlock = mempool.getPendingTransactions().size();

        for (int i = 0; i < numberOfTransactionPerBlock; i++) {
            transactions.add(mempool.getPendingTransactions().get(i));
            rewardFee += mempool.getPendingTransactions().get(i).getFee();
        }

        transactions.add(new Transaction("Reward " + (reward + rewardFee) + " for " + name, 0));

        return transactions;

    }

}
