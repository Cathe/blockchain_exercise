package Transactions;

import java.nio.charset.Charset;

public class Transaction{

    private String content;
    private double fee;

    public Transaction(String content, double fee){
        this.content = content;
        this.fee = fee;
    }

    public String getContent() {
        return content;
    }

    public double getFee() {
        return fee;
    }

    public byte[] getBytes(Charset utf8) {
        return (content + fee).getBytes(utf8);
    }

    @Override
    public String toString() {
        return "Transactions.Transaction{" +
                "content='" + content + '\'' +
                ", fee=" + fee +
                '}';
    }
}
