package Transactions;

import java.util.Comparator;

public class TransactionComparator implements Comparator<Transaction> {
    @Override
    public int compare(Transaction t1, Transaction t2) {

        return (int)(t2.getFee()-t1.getFee());
    }
}
