import Network.Network;
import Nodes.Miner;
import Nodes.Node;
import Nodes.TransactionPlatform;
import Transactions.Transaction;

public class Main {
    public static void main(String[] args) {

        //INITIAL STRUCTURE

        Network network = new Network();
        Node initialNode = new Node(network);
        network.register(initialNode);


        //BLOCKCHAIN LIFECYCLE

        Miner bob = new Miner("Bob", network);

        TransactionPlatform bitPay = new TransactionPlatform(network);

        bitPay.upload(initialNode);
        bob.upload(bitPay);

        bitPay.addTransaction(new Transaction("Luis 50.- -> Ella", 2));
        bitPay.addTransaction(new Transaction("Ella 43.- -> Luis", 5));
        bitPay.addTransaction(new Transaction("Marie 24.- -> Jackes", 3));
        bitPay.addTransaction(new Transaction("Jean 86.- -> Louis", 5));
        bitPay.addTransaction(new Transaction("Arnold 42.- -> Ella", 7));

        bob.mine();

        bitPay.addTransaction(new Transaction("Mark 24.- -> Henri", 1));
        bitPay.addTransaction(new Transaction("Paul 51.- -> Damien", 5));
        bitPay.addTransaction(new Transaction("Ella 30.- -> Luis", 2));
        bitPay.addTransaction(new Transaction("Luis 50.- -> Ella", 3));

        bob.mine();

        System.out.println(initialNode);


    }
}
