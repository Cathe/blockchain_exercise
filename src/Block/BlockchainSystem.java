package Block;

public class BlockchainSystem {

    private static int blocNumber = 1;

    private int numberOfTransactionPerBlock = 4;

    private int minningReward = 6;

    private String version = "0x20000000";

    private int difficulty = 16;


    public int getNumberOfTransactionPerBlock() {
        return numberOfTransactionPerBlock;
    }

    public int getMinningReward() {
        return minningReward;
    }

    public String getVersion() {
        return version;
    }

    public int getBlocNumber() {
        return blocNumber;
    }

    public int getDifficulty() {
        return difficulty;
    }
}
