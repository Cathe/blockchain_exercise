package Block;

import Transactions.Transaction;

import java.util.List;

public class Block {

    //HEADER
    private String previousBlockHash;
    private String merkleRoot;
    private long timeStamp;
    private int difficulty;
    private long nonce;

    //DATA
    private List<Transaction> data;
    private int blockNumber;
    private String hash;


    public Block(
            int blockNumber,
            String previousBlockHash,
            String merkleRoot,
            long timeStamp,
            int difficulty,
            List<Transaction> data) {

        this.blockNumber = blockNumber;
        this.previousBlockHash = previousBlockHash;
        this.merkleRoot = merkleRoot;
        this.timeStamp = timeStamp;
        this.difficulty = difficulty;
        this.data = data;
    }


    public void setNonce(long nonce) {
        this.nonce = nonce;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getPreviousBlockHash() {
        return previousBlockHash;
    }

    public String getMerkleRoot() {
        return merkleRoot;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public List<Transaction> getData() {
        return data;
    }

    public String getHash() {
        return hash;
    }

    @Override
    public String toString() {

        String result =  "Block.Block{" +
                "blockNumber=" + blockNumber +
                ", previousBlockHash='" + previousBlockHash + '\'' +
                ", merkleRoot='" + merkleRoot + '\'' +
                ", timeStamp=" + timeStamp +
                ", difficulty=" + difficulty +
                ", nonce=" + nonce +
                ", hash='" + hash + '\'' +
                '}';

        System.out.println(result);

        for (Transaction t : data) {
            System.out.println(t);
        }

        return "";
    }
}
