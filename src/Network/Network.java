package Network;

import Block.Block;
import Nodes.Observer;
import Transactions.Transaction;

import java.util.ArrayList;
import java.util.List;

public class Network implements Subject {

    List<Observer> nodes = new ArrayList<>();

    @Override
    public void register(Observer o) {
        nodes.add(o);
    }

    @Override
    public void remove(Observer o) {
        nodes.remove(o);
    }

    @Override
    public void notifyBlockAdded(Block addedBlock) {
        for (Observer node : nodes) {
            node.updateBlockchain(addedBlock);
        }
    }

    @Override
    public void notifyTransactionAdded(Transaction addedTransaction) {
        for (Observer node : nodes) {
            node.updateMempool(addedTransaction);
        }
    }
}
