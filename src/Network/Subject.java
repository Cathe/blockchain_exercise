package Network;

import Block.Block;
import Nodes.Observer;
import Transactions.Transaction;

public interface Subject {

    void register(Observer o);
    void remove(Observer o);
    void notifyBlockAdded(Block addedBlock);
    void notifyTransactionAdded(Transaction addedTransaction);
}
